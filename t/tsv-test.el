;;; tsv-test.el ---   -*- lexical-binding: t --*--

;; Copyright (C) 2021, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20210207
;; Updated: 20210207
;; Version: 0.0
;; Keywords: 

;;; Commentary:

;;; Change Log:

;;; Code:

  
(ert-deftest tsv-test--basic ()
  (let ((data-filename
         (concat (file-name-directory (symbol-file 'tsv))
                 (file-name-as-directory "t")
                 "tsv-data.tsv"))
        )
    (should
     (equal
      "175"
      (let ((hash
             (tsv/file-as-hash data-filename "feature" "peach" :skip-re "#")))
        (gethash "weight" hash))
      ))
    ))


(ert-deftest tsv-test--alist-map ()
  (let ((data-filename
         (concat (file-name-directory (symbol-file 'tsv))
                 (file-name-as-directory "t")
                 "tsv-data.tsv"))
        )
    (should
     (equal
      '(("weight" . "175") ("cost" . "500") ("height" . "4") ("calorie" . "39"))
      (tsv/file-as-alist data-filename "feature" "peach" :skip-re "#")
      ))))


(ert-deftest tsv-test--val-type ()
  (let ((data-filename
         (concat (file-name-directory (symbol-file 'tsv))
                 (file-name-as-directory "t")
                 "tsv-data.tsv"))
        )
    (should
     (equal
      175
      (let ((hash
             (tsv/file-as-hash data-filename "feature" "peach" :skip-re "#" :val-type 'integer)))
        (gethash "weight" hash))
      ))
    ))


(ert-deftest tsv-test--default-val ()
  (let ((data-filename
         (concat (file-name-directory (symbol-file 'tsv))
                 (file-name-as-directory "t")
                 "tsv-data.tsv"))
        )
    (should
     (equal
      "999"
      (let ((hash
             (tsv/file-as-hash data-filename "feature" "mikan"
                               :skip-re "#" :default-val "999" :miss-val-mark "-")))
        (gethash "height" hash))
      ))
    ))


(ert-deftest tsv-test--val-field-nil ()
  (let ((data-filename
         (concat (file-name-directory (symbol-file 'tsv))
                 (file-name-as-directory "t")
                 "tsv-data.tsv"))
        )
    (should
     (equal
      (qw calorie cost height weight)
      (let ((hash
             (tsv/file-as-hash data-filename "feature" nil :skip-re "#")))
        (sort
         (map-keys hash)
         #'string<)
      )))))



(ert-deftest tsv-test--miss-val-col-ok ()
  (let ((data-filename
         (concat (file-name-directory (symbol-file 'tsv))
                 (file-name-as-directory "t")
                 "tsv-data--miss-val.tsv"))
        )
    (should
     (equal
      99
      (let ((hash
             (tsv/file-as-hash data-filename "feature" "banana"
                          :skip-re "#" :miss-val-col-ok? t :val-type 'integer :default-val 99)))
            (gethash "height" hash)
            )))))


(ert-deftest tsv-test--unknown-opt-name-gives-error ()
  (let ((data-filename
         (concat (file-name-directory (symbol-file 'tsv))
                 (file-name-as-directory "t")
                 "tsv-data--miss-val.tsv"))
        )
    (should-error
     (tsv/file-as-hash data-filename "feature" "banana"
                       ;; intentionally mistyped :miss-val-col-ok?
                       :skip-re "#" :mis-val-col-ok? t :val-type 'integer :default-val 99
            ))))


(ert-deftest tsv-test--miss-val-col-ng ()
  (let ((data-filename
         (concat (file-name-directory (symbol-file 'tsv))
                 (file-name-as-directory "t")
                 "tsv-data--miss-val.tsv"))
        )
    (should-error
     (tsv/file-as-hash data-filename "feature" "banana"
                       :skip-re "#" :val-type 'integer :default-val 99)
     )))
  


(ert-deftest tsv-test--val-mapper ()
  (let ((data-filename
         (concat (file-name-directory (symbol-file 'tsv))
                 (file-name-as-directory "t")
                 "tsv-data.tsv"))
        )
    (should
     (equal
      (qw CALORIE COST HEIGHT WEIGHT)
      (let ((hash
             (tsv/file-as-hash data-filename "feature" nil :skip-re "#" :key-mapper #'upcase)))
        (sort
         (map-keys hash)
         #'string<)
      )))))


(ert-deftest tsv-test--key-mapper ()
  (let ((data-filename
         (concat (file-name-directory (symbol-file 'tsv))
                 (file-name-as-directory "t")
                 "tsv-data.tsv"))
        )
    (should
     (equal
      (sqrt 500.0)
      (let ((hash
             (tsv/file-as-hash data-filename "feature" "peach" :skip-re "#" :val-type 'float :val-mapper #'sqrt)))
        (gethash "cost" hash)
      )))))



;;; tsv-test.el ends here
