;;; tsv.el --- utils for handling tab separated fields data  -*- lexical-binding: t; byte-compile-docstring-max-column: 104 -*-

;; Copyright (C) 2020, Paul Horton.
;; License: This software may be used and/or distributed under the GNU General Public License
;; Contact:  ,(concat [112 97 117 108 104 64 105 115 99 98 46 111 114 103])

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20200329
;; Updated: 20240613
;; Version: 0.0
;; Keywords: tab separated fields

;;; Commentary:

;;  utilities for dealing with data in a tab separated fields format

;;; Change Log:

;;; Code:


(require 'map)
(require 'forlines)
(require 'shortcuts)
(require 'plist-opts)


(defvar tsv/default-delim "\t" "default field delimiter")

(defun tsv/delim (opts)
  "Return field delimiter given in opts or default-delim"
  (or (plist-opts/remove opts :delim) tsv/default-delim))

(defvar tsv/default-miss-val-mark "" "default missing value mark")

(defun tsv/miss-val-mark (opts)
  "Return string representing missing values given in opts;
or, by default, miss-val-mark"
  (or (plist-opts/remove opts :miss-val-mark) tsv/default-miss-val-mark))


(defun tsv/transpose-region (region ask-delim?)
  "Transpose REGION.  With prefix argument ask which delimiter to use"
  ;;.... Just a stub for now
  ;; Maybe first implement one or two internal representations of tsv data
  ;; Pry using a vector/list of vectors/lists
  ;; Then write separate to read that data in from a region
  ;; and to transpose that data
  ;;
  ;; Until then, 以下のshell-command-on-regionで我慢してね。
  ;;  Transpose rows and columns.  'chop' removes trailing tab characters.
  ;;  % perltab -e '$T[$_] .= "$F[$_]\t" for 0..$#F'  -z 'chop, say  for @T'
  (ignore region ask-delim?); avoid byte-compiler warning
)


(defun tsv/slurp-field (field-name-or-num &rest opts)
  "Return data field FIELD-NAME-OR-NUM as list"
  (plist-opts/strip-extra-paren opts)
  (let  ((ret-list))
    (tsv/apply-to-field
     (lambda (field)
       (push field ret-list))
     field-name-or-num opts)
    ret-list
    ))

(defun tsv/apply-to-field (functor field-name-or-num &rest opts)
  "Apply functor to field FIELD-NAME-OR-NUM of tab separated buffer."
  (plist-opts/strip-extra-paren opts)
  (let* (; options
         (delim              (tsv/delim opts))
         (head?          (or (plist-opts/remove opts :head?) (stringp field-name-or-num)))
         (field-col-num      (tsv/field-col-num field-name-or-num "tsv/apply-to-field" delim opts))
         )
    (when head?
      (unless (plist-member opts :skip-num)
        (push2 :skip-num 1 opts)))
    (tsv/forlines
     (lambda (fields)
       (funcall functor (nth field-col-num fields))
       )
     opts
     )))


(defun tsv/file-as-hash (pathname key-field val-field &rest opts)
  "Return hash populated with data from tab separated fields file PATHNAME.

Mandatory arguments:
  PATHNAME  path to data file
  KEY-FIELD  column number or field name, the field to use as key
  VAL-FIELD  column number or field name, the field to use as value

Optional arguments:
  See `tsv/buffer-as-hash'
"
  (with-temp-buffer
    (insert-file-contents pathname)
    (tsv/buffer-as-hash key-field val-field opts)))


(defun tsv/file-as-alist (pathname key-field val-field &rest opts)
  "Return alist populated with data from tab separated fields file PATHNAME.

Mandatory arguments:
  PATHNAME  path to data file
  KEY-FIELD  column number or field name, the field to use as key
  VAL-FIELD  column number or field name, the field to use as value

Optional arguments:
  See `tsv/buffer-as-alist'
"
  (with-temp-buffer
    (insert-file-contents pathname)
    (tsv/buffer-as-alist key-field val-field opts)))



(defun tsv/buffer-as-hash (key-field val-field &rest opts)
  "Return hash populated with data from tab separated fields in current buffer

Mandatory arguments:
  KEY-FIELD  column number or field name, the field to use as key
  VAL-FIELD  column number or field name, the field to use as value

Optional arguments:
  :hash   Update values in HASH instead of starting from a fresh one.
  For other options see `tsv/map-put'
"
  (plist-opts/strip-extra-paren opts)
  (let1  H  (or
             (plist-opts/remove opts :hash)
             (make-hash-table :test #'equal))
    (tsv/map-put H key-field val-field opts)))


(defun tsv/buffer-as-alist (key-field val-field &rest opts)
  "Return alist populated with data from tab separated fields in current buffer

Mandatory arguments:
  KEY-FIELD  column number or field name, the field to use as key
  VAL-FIELD  column number or field name, the field to use as value

Optional arguments:
  :alist   Add to ALIST instead of starting from a fresh one
  For other options see `tsv/map-put'
"
  (plist-opts/strip-extra-paren opts)
  (let1  alist  (or
                 (plist-opts/remove opts :alist)
                 nil)
    (tsv/map-put alist key-field val-field opts)))



(defun tsv/map-put (map key-field val-field &rest opts)
  "Use contents of the indicated columns to add key,value pairs to map MAP

Mandatory arguments:
  MAP      map to place (key,values) in
  KEY-FIELD  column number or field name, the field to use as key
  VAL-FIELD  column number or field name, the field to use as value
             nil is also allowed, indicating to always use :default-val

Options:
  :default-val    value to use when value is missing, defaults to t
  :delim          Field separation delimiter, defaults to `tsv/default-delim'
  :head?          Does the first (non-comment) line of the data contain column names?
                  When not stipulated, head? is set to: (or (stringp key-col) (stringp val-col))
  :val-type       Cast value to type val-type.  Supported types are integer and float.
  :miss-key-col-ok?  If true, lines with fewer fields than key-col-num are silently ignored,
                     otherwise an error is produced.
  :miss-val-col-ok?  If true, lines with fewer fields than val-col-num are treated as missing values,
                     otherwise an error is produced.
  :miss-val-mark  String indicating missing value, defaults to empty string.
  :miss-val-ok?   When given, lines missing values are silently ignored; otherwise an error is produced.
  :key-mapper     When given, (funcall key-mapper (value)) is used for hash key
  :val-mapper     When given, (funcall val-mapper (value)) is used for hash value


When not given, the value :miss-val-ok? is set to t
when val-field is null, or when :default-val or :miss-val-mark are given.


OPTS is forwarded to 'forlines-call', so also see that documentation.
"
  (or key-field (error "tsv/map-put recieved nil key-field"))
  (or (stringp key-field) (integerp key-field) (null key-field)
      (error "tsv/map-put recieved invalid key-field %S" key-field))
  (or (stringp val-field) (integerp val-field) (null val-field)
      (error "tsv/map-put recieved invalid val-field %S" val-field))
  (plist-opts/strip-extra-paren opts)
  (or (cl-evenp (length opts))
      (error "tsv/map-put received opts with odd number of elements: %S" opts))
  ;;
  (when (or (plist-member opts :default-val) (plist-member opts :miss-val-mark))
    (unless (plist-member opts :miss-val-ok?)
      (push2 :miss-val-ok? t opts)
      ))
  ;;
  ;; Compute column numbers for key-field and val-field
  (let (; options
        (val-type           (plist-opts/remove opts :val-type))
        (head?          (or (plist-opts/remove opts :head?) (stringp key-field) (stringp val-field)))
        (default-val    (or (plist-opts/remove opts :default-val) t))
        (delim              (tsv/delim opts))
        (miss-key-col-ok?   (plist-opts/remove opts :miss-key-col-ok?))
        (miss-val-col-ok?   (plist-opts/remove opts :miss-val-col-ok?))
        (miss-val-mark      (tsv/miss-val-mark opts))
        (miss-val-ok?       (plist-opts/remove opts :miss-val-ok?))
        (val-mapper         (plist-opts/remove opts :val-mapper))
        (key-mapper         (plist-opts/remove opts :key-mapper))
        )
    (when (and val-mapper (not val-field))
      (error "tsv/map-put, :val-mapper option (%S) given with nil val-field argument" val-mapper))
    ;; Get column numbers of fields.  Except val-col-num may be nil.
    (let ((key-col-num
           (tsv/field-col-num key-field "tsv/map-put" delim opts))
          (val-col-num
           (and val-field
                (tsv/field-col-num val-field "tsv/map-put" delim opts)))
          )
      ;; Read and process data file
      (when head?
        (unless (plist-member opts :skip-num)
          (push2 :skip-num 1 opts)))
      (let (key val)
        (tsv/forlines
         (lambda (fields)
           (setq key (nth key-col-num fields))
           (if (not key)
               (unless miss-key-col-ok? (user-error "tsv/map-put; for key field %S, expected %d fields but got: '%s'"  key-field key-col-num fields))
             (setq val
                   (if val-col-num
                       (if-let ((val-string (nth val-col-num fields)))
                           (if (string= val-string miss-val-mark)
                               (if miss-val-ok?
                                   default-val
                               (user-error "tsv/map-put; for value field %S, missing value in fields '%s'" val-field fields)
                               )
                             val-string
                             )
                         (if miss-val-col-ok?
                             default-val
                           (user-error "tsv/map-put; for value field %S, expected %d fields but got: '%s'"  val-field val-col-num fields)
                           ))
                     default-val
                     ))
             (cl-case val-type
               ('integer
                (unless (integerp val);; val via default-val may not be a string.
                  (if (stringp val)
                      (setq val (cl-parse-integer val))
                    (error "tsv/map-put; cannot convert val %S to integer" val)
                    )))
               ('float
                (unless (floatp val)
                  (setq val (float (string-to-number val))))
                )
               ('nil t)
               (t
                (error "tsv/map-put; unsupported val-type '%S'" val-type))
               )
             (let ((key-to-use (if key-mapper (funcall key-mapper key)  key))
                   (val-to-use (if val-mapper (funcall val-mapper val)  val)))
               (if (hash-table-p map)
                   (setf (map-elt map key-to-use) val-to-use)
                 (push (cons key-to-use val-to-use) map)
                 ))
           ))
         opts
         )
        (if (hash-table-p map)  map  (reverse map))
        ))))



(defun tsv/forlines (functor &optional opts)
  "Like forlines, but provides list variable 'fields' for function FUNCTOR"
  (let ((delim  (tsv/delim opts))
        fields
        )
    (forlines-call
     (lambda (line)
       (setq fields (split-string line delim))
       (funcall functor fields)
       )
     opts
     )))



(defun tsv/field-col-num (field-name-or-num caller-name delim forlines-opts)
  "Return column number matching field FIELD-NAME-OR-NUM.

If FIELD-NAME-OR-NUM is an integer, no work to do so return fields as is.

Otherwise FIELD-NAME-OR-NUM should be a field name.
Its column number is returned."
  (cl-assert (stringp delim))
  (if (integerp field-name-or-num)
      field-name-or-num
    (tsv/col-num←field-name
     (tsv/field-names delim forlines-opts)
     field-name-or-num
     caller-name
     )
    ))



(defun tsv/col-num←field-name (field-names field-name-or-num caller-name)
  "Return position of FIELD-NAME-OR-NUM in sequence FIELD-NAMES.
If FIELD-NAME-OR-NUM is an in-range integer, just return it.

CALLER-NAME is used for error messages."
  (cl-typecase field-name-or-num
    (integer
     (cl-assert (≦ 0 field-name-or-num) t
                "In %s; field-name-or-num num %d negative" caller-name field-name-or-num
                )
     (cl-assert (< field-name-or-num (length field-names)) t
                "In %s; field-name-or-num num %d ≧ number of field-names %S" caller-name field-name-or-num field-names)
     field-name-or-num
     )
    (string
     (or (cl-position field-name-or-num field-names :test #'string=)
         (error "In %s; field name %s not found in field-names %S" caller-name field-name-or-num field-names))
     )
    (t
     (error "Internal error. Called from %s; tsv/col-num encountered field %S of unknown type"
            caller-name field-name-or-num
            ))
    ))


(defun tsv/field-names (delim forlines-opts)
  "head line from current buffer"
  (forlines-call
      (lambda (line)  (split-string line delim))
    (append '(:keep-num 1) forlines-opts)
  ))


(cl-defun tsv/insert-hash (hash &optional (format-spec "%S\t%S\n"))
  "Insert hash HASH in current buffer as according to format-spec"
  (maphash
   (lambda (key val)
     (insert (format format-spec key val)))
     hash))



(provide 'tsv)

;;; tsv.el ends here
